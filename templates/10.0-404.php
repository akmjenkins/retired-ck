<?php $bodyclass = 'error404'; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
	
		<div class="sw">
			<div class="hgroup">
				<h1 class="hgroup-title">404 Error</h1>
				<span class="hgroup-subtitle">Curabitur in Sapien Finibus</span>
			</div><!-- .hgroup -->
		</div><!-- .sw -->
		
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="home">Home</a>
				<a href="#">404 Error</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<div class="sw">
		
			<div class="main-body">
			
				<div class="content center">
					<div class="article-body">
						
						<p>
							The page you're looking for seems to be missing but no need to worry.
						</p>
							 
						<p>
							You can search our site to try and find what you're looking for, click the back button to return to the page you were on previously, or try using the navigation above.
						</p>
							 
						<p>
							If you keep getting this error page, please contact us and let us know.
						</p>
						
						<div class="error404-actions">
						
							<form action="/" class="single-form search-form">
								<div class="fieldset">
									<input type="text" name="s" placeholder="Type your search here...">
									<button class="t-fa-abs fa-search">Search</button>
								</div><!-- .fieldset -->
							</form>
							
							<a href="#" class="button red">Go Back</a>
						
						</div><!-- .error404-actions -->
						
					</div><!-- .article-body -->
				</div><!-- .content -->
				
			</div><!-- .main-body -->
		
		</div><!-- .sw -->
		
	</section>
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>