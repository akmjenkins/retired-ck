<div class="login-register-tabs">

	<button class="close">Close <i class="fa fa-close"></i></button>

	<div class="tab-wrapper">

		<div class="tab-controls">
		
			<div class="selector with-arrow">
				<select class="tab-controller">
					<option value="login">Login</option>
					<option value="register">Register</option>
				</select>
				<span class="value">&nbsp;</span>
			</div><!-- .selector -->
		
			<div class="tab-control-wrap">
				<button class="button tab-control selected">Log In</button>
				<button class="button tab-control">Register</button>
			</div><!-- .tab-control-wrap -->
		
		</div><!-- .tab-controls -->
		
		<div class="tab-holder">
			
			<div class="tab selected">
			
				<h2>Login</h2>
				<?php include('i-login-form.php'); ?>
				
			</div><!-- .tab -->
			
			<div class="tab">
			
				<h2>Register</h2>
				<?php include('i-register-form.php'); ?>
				
			</div><!-- .tab -->
			
		</div><!-- .tab-holder -->
		
	</div><!-- .tab-wrapper -->
</div><!-- .login-register-tabs -->