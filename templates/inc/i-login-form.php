<form action="/" class="body-form">

	<div class="fieldset">
	
		<div class="field-wrap t-fa-abs fa-user">
			<input type="email" name="email" placeholder="E-mail address">
		</div>
		
		<div class="field-wrap t-fa-abs fa-lock">
			<input type="password" name="password" placeholder="Password">
		</div>
		
		<button class="red button">Submit</button>
		
		<div class="f-right alright">
			Did you forget your password? <br />
			<a href="#" class="inline">Click here to reset it</a>
		</div>
	
	</div><!-- .fieldset -->
</form>