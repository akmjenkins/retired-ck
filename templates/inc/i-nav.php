<button class="toggle-nav">
	<span>&nbsp;</span> Menu
</button>

<div class="nav">
	<div class="sw">
		
		<nav>
			<ul>
				<li><a href="#">Home</a></li> <!-- This link appears on the small nav (when scrolling) only -->
				<li><a href="#">Retired Workers</a></li>
				<li><a href="#">Employment</a></li>
				<li><a href="#">Employers</a></li>
				<li><a href="#">News</a></li>
			</ul>
			
			<div class="btngroup">
				<a href="./inc/i-login-signup-modal.php" class="login-modal">Login</a>
				<a href="./inc/i-login-signup-modal.php" class="register-modal">Register</a>
			</div><!-- .btn-group -->
			
		</nav>
	
		<div class="nav-top">
		
			<a href="#" class="t-fa-abs fa-book nav-tooltip" title="Resources">Resources</a>
			<a href="#" class="t-fa-abs fa-envelope nav-tooltip" title="Contact">Contact</a>
			<button class="t-fa-abs fa-search nav-tooltip toggle-search" title="Search">Search</button>
			
			<?php include('i-social.php'); ?>
			
		</div><!-- .nav-top -->
	</div><!-- .sw -->
</div><!-- .nav -->