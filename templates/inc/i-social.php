<div class="social">
	<a href="#" title="Like Retired Workers on Facebook" class="social-fb" rel="external">Like Retired Workers on Facebook</a>
	<a href="#" title="Follow Retired Workers on Twitter" class="social-tw" rel="external">Follow Retired Workers on Twitter</a>
	<a href="#" title="Add Retired Workers to your Circles on Google Plus" class="social-gp" rel="external">Add Retired Workers to your Circles on Google Plus</a>
	<a href="#" title="Check out Retired Workers on Instagram" class="social-ig" rel="external">Check out Retired Workers on Instagram</a>
	<a href="#" title="Connect With Retired Workers on LinkedIn" class="social-in" rel="external">Connect with Retired Workers on LinkedIn</a>
	<a href="#" title="Watch Retired Workers' YouTube Channel" class="social-yt" rel="external">Watch Retired Workers' YouTube Channel</a>
</div><!-- .social -->