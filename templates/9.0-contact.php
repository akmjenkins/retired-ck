<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">

	<section>
	
		<div class="sw">
			<div class="hgroup">
				<h1 class="hgroup-title">Contact</h1>
				<span class="hgroup-subtitle">Curabitur in Sapien Finibus</span>
			</div><!-- .hgroup -->
		</div><!-- .sw -->
		
		<div class="breadcrumbs">
			<div class="sw">
				<a href="#" class="home">Home</a>
				<a href="#">Contact</a>
			</div><!-- .sw -->
		</div><!-- .breadcrumbs -->
		
		<div class="sw">

			<div class="main-body">
				<div class="content">
				
					<div class="article-body">
					
						<p class="excerpt">
							Quisque a odio vel mauris suscipit venenatis in ut massa. Sed sed magna id ipsum mattis sodales in eu lacus. In ullamcorper mattis rutrum. Cras augue odio, accumsan sed aliquet id, mollis in sem. Vivamus maximus ac arcu nec fringilla. Quisque euismod lacus vel consectetur dignissim.
						</p>
						
						<p>
							Vivamus vel metus vel dolor viverra sodales. Donec in convallis odio. Curabitur in accumsan ante. Donec id auctor elit, eu dictum massa. Donec ut vehicula tortor. Donec at nisi varius, lacinia nunc mollis, scelerisque nisi.
						</p>
					
					</div><!-- .article-body -->
				
				</div><!-- .content -->
			</div><!-- .main-body -->
		
		</div><!-- .sw -->
		
	</section>

	<section class="d-bg grey-bg split-block with-border">
		<div class="split-block-item">
			<div class="split-block-content">
				<h3>Contact Us</h3>
				
				<form action="/" class="body-form">
					<div class="fieldset">
					
						<div class="field-wrap t-fa-abs fa-user">
							<input type="text" name="name" placeholder="Full Name">
						</div>
					
						<div class="field-wrap t-fa-abs fa-envelope">
							<input type="email" name="email" placeholder="E-mail address">
						</div>
						
						<div class="field-wrap t-fa-abs ta-wrap fa-pencil">
							<textarea name="message" placeholder="Message"></textarea>
						</div>
						
						<button class="red button">Submit</button>
						
						<div class="f-right alright">
							Did you forget your password? <br />
							<a href="#" class="inline">Click here to reset it</a>
						</div>
					
					</div><!-- .fieldset -->
				</form>

				
			</div><!-- .split-block-content -->
		</div><!-- .split-block-item -->
		
		<div class="split-block-item">
			<div class="split-block-content">
			
				<h3>Location</h3>
				
				<div class="grid">
				
					<div class="col col-2 xs-col-1">
						<div class="item">
							<address>
								123 This Street <br />
								St. John's, NL A1B 2C3
							</address>
						</div>
					</div>
					
					<div class="col col-2 xs-col-1">
						<div class="item">
							<div class="rows">
								<div class="row">
									<span class="l">Phone:</span>
									<span class="r">1 (709) 123-4567</span>
								</div><!-- .row -->
								<div class="row">
									<span class="l">Fax:</span>
									<span class="r">1 (709) 123-4567</span>
								</div><!-- .row -->
							</div><!-- .rows -->
						</div><!-- .item -->
					</div><!-- .col -->
					
					<div class="col col-1">
						<div class="item embedded-gmap-wrap">
							<div class="embedded-gmap">
								<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1346.074908850923!2d-52.71411039999999!3d47.56487340000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4b0ca3bcf6218969%3A0x30b59d2714d7bd39!2s82+Harvey+Rd%2C+St.+John&#39;s%2C+NL+A1C+2G1!5e0!3m2!1sen!2sca!4v1423853227467" frameborder="0" style="border:0"></iframe>
							</div><!-- .embedded-gmap -->
						</div><!-- .item -->
					</div><!-- .col -->
				</div><!-- .grid -->
				
			</div><!-- .split-block-content -->
		</div><!-- .split-block-item -->
		
	</section><!-- .split-block -->
	
</div><!-- .body -->

<?php include('inc/i-footer.php'); ?>